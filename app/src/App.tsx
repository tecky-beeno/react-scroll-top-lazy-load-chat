import React, {
  DOMAttributes,
  useEffect,
  useLayoutEffect,
  useState,
} from 'react'
import logo from './logo.svg'
import './App.css'
import { IonInput, IonButton, IonContent } from '@ionic/react'

type Msg = { id: number; content: string; is_self: boolean }
function sampleMsgList() {
  const msgList = [
    { id: 1, content: 'Hi', is_self: false },
    { id: 2, content: "I'm here", is_self: true },
  ]
  for (let i = 0; i < 50; i++) {
    let msg = msgList[i]
    msgList.push({
      ...msg,
      content: msg.content + ' ' + i,
      id: msgList.length + 1,
    })
  }
  return msgList
}

function App() {
  const msgList = useState<Msg[]>(sampleMsgList)
  const newMsg = useState('')
  const lastScrollMsgId = useState(0)
  const scrollTop = useState(0)
  const canAutoLoad = useState(true)
  function sendMessage() {
    const content = newMsg[0]
    if (!content) return
    const list = msgList[0]
    msgList[1]([
      ...list,
      { id: list.length + 1, content, is_self: !content.startsWith('fake:') },
    ])
    newMsg[1]('')
  }
  function loadMoreMessages() {
    if (!canAutoLoad[0]) return
    canAutoLoad[1](false)
    setTimeout(() => {
      canAutoLoad[1](true)
    }, 1000)
    msgList[1](list => {
      for (let i = 0; i < 6; i++) {
        const msg = { ...list[0] }
        msg.id--
        msg.content = 'old message: ' + msg.id
        list = [msg, ...list]
      }
      return list
    })
  }
  // or use https://ionicframework.com/docs/api/refresher
  useEffect(() => {
    if (scrollTop[0] > 400) return
    loadMoreMessages()
  }, [scrollTop[0], canAutoLoad[0]])
  return (
    <ion-app>
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot="start">
            <ion-back-button default-href="/"></ion-back-button>
          </ion-buttons>
          <ion-title>chatroom ({scrollTop[0]})</ion-title>
          <ion-buttons slot="end">
            <IonButton onClick={loadMoreMessages}>
              <ion-icon name="add"></ion-icon>
            </IonButton>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>
      <IonContent
        className="ion-padding"
        scrollEvents
        onIonScroll={e => {
          scrollTop[1](e.detail.scrollTop)
          if (e.detail.scrollTop === 0) {
            let elem = (e.target as HTMLElement).shadowRoot?.querySelector(
              'main',
            )
            if (elem) {
              elem.scrollTop = 0
            }
          }
        }}
      >
        <div className="chatroom">
          <div className="message-list">
            {msgList[0].map((msg, i, msgList) => (
              <div
                className={'message-box ' + (msg.is_self ? 'self' : 'other')}
                key={msg.id}
                ref={e => {
                  if (
                    !e ||
                    i != msgList.length - 1 ||
                    !msg.is_self ||
                    lastScrollMsgId[0] == msg.id
                  ) {
                    return
                  }
                  setTimeout(
                    () => {
                      e.scrollIntoView({ behavior: 'smooth' })
                      lastScrollMsgId[1](msg.id)
                    },
                    lastScrollMsgId[0] === 0 ? 1000 : 0,
                  )
                }}
              >
                <div className="message">{msg.content}</div>
              </div>
            ))}
          </div>
        </div>
      </IonContent>
      <ion-footer class="ion-padding">
        <ion-row>
          <ion-col>
            <IonInput
              placeholder="Type message here ..."
              value={newMsg[0]}
              onIonChange={e => newMsg[1](e.detail.value || '')}
              onKeyPress={e => {
                if (e.key === 'Enter') {
                  sendMessage()
                }
              }}
            ></IonInput>
          </ion-col>
          <ion-col size="auto">
            <IonButton
              shape="round"
              size="small"
              onClick={sendMessage}
              disabled={!newMsg[0]}
            >
              <ion-icon name="paper-plane"></ion-icon>
            </IonButton>
          </ion-col>
        </ion-row>
      </ion-footer>
    </ion-app>
  )
}

export default App

type CustomElement<T> = Partial<T & DOMAttributes<T> & { children: any }>

declare global {
  namespace JSX {
    interface IntrinsicElements {
      [tag: string]: CustomElement<any>
      // ['ion-app']: CustomElement<any>
      // ['ion-header']: CustomElement<any>
      // ['ion-toolbar']: CustomElement<any>
      // ['ion-buttons']: CustomElement<any>
      // ['ion-back-button']: CustomElement<any>
      // ['ion-title']: CustomElement<any>
      // ['ion-content']: CustomElement<any>
      // ['ion-input']: CustomElement<any>
      // ['ion-button']: CustomElement<any>
      // ['ion-footer']: CustomElement<any>
    }
  }
}
